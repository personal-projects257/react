package com.cctns.dataExtractionUtility;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
//@SpringBootApplication(exclude={DataSourceAutoConfiguration.class})
public class DataExtractionUtility1Application {

	public static void main(String[] args) {
		SpringApplication.run(DataExtractionUtility1Application.class, args);
	}

}
