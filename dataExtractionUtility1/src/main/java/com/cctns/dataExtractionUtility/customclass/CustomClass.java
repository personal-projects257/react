package com.cctns.dataExtractionUtility.customclass;

import java.util.Map;
import java.util.concurrent.CopyOnWriteArrayList;

public class CustomClass {
 
	private CopyOnWriteArrayList<CopyOnWriteArrayList<Map<String,Object>>> copyOnWriteArrayDataCollection=new CopyOnWriteArrayList<CopyOnWriteArrayList<Map<String,Object>>>();

	public CustomClass() {
		
	}
	
	
	public  CopyOnWriteArrayList<CopyOnWriteArrayList<Map<String,Object>>> getOutPutValueForTheQueryData()
	{
		return  copyOnWriteArrayDataCollection;
	}
	
	
	
}
