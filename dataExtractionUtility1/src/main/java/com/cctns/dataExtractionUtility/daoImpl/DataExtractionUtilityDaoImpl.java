package com.cctns.dataExtractionUtility.daoImpl;

import java.util.Map;
import java.util.concurrent.Callable;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.ThreadPoolExecutor;

import org.springframework.jdbc.core.JdbcTemplate;

import com.cctns.dataExtractionUtility.customclass.CustomClass;

public class DataExtractionUtilityDaoImpl implements Callable<Object> {
	
	private JdbcTemplate stateJDBCTemplate;
	private CustomClass customclass;
	private ThreadPoolExecutor executor;
	private String finalQuery;
	
 
	

	public DataExtractionUtilityDaoImpl(JdbcTemplate stateJDBCTemplate, CustomClass customclass,
			ThreadPoolExecutor executor, String finalQuery) {
		  this.stateJDBCTemplate=stateJDBCTemplate;
		  this.customclass=customclass;
		  this.executor=executor;
		  this.finalQuery=finalQuery;
		  
	}
	
	@Override
    public Object call() {
        try{
           extractData();
        }
         catch (Exception e) {
           System.out.println("Error While Extracting the data "+e.getMessage());
        }
        
        return 1; // simply returning as this call() cannot be made as void
    }
	
	
	public  void extractData() {
		CopyOnWriteArrayList<Map<String,Object>> copyOnWriteArrayDataCollection=new CopyOnWriteArrayList<Map<String,Object>>();
		  
		  copyOnWriteArrayDataCollection= (CopyOnWriteArrayList<Map<String, Object>>) stateJDBCTemplate.queryForList(finalQuery);
		  customclass.getOutPutValueForTheQueryData().add(copyOnWriteArrayDataCollection);
		  
	}
 
	
}
