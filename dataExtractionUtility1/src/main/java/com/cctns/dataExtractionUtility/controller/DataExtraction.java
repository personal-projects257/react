package com.cctns.dataExtractionUtility.controller;

import java.sql.Connection;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.ResourceBundle;
import java.util.concurrent.ConcurrentHashMap;

import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.ThreadPoolExecutor;

import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.datasource.DriverManagerDataSource;

import javax.sql.DataSource;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.cctns.dataExtractionUtility.customclass.CustomClass;
import com.cctns.dataExtractionUtility.daoImpl.DataExtractionUtilityDaoImpl;

@RestController
@RequestMapping("/data")
public class DataExtraction {

    ConcurrentHashMap<String, Boolean> stateMap = null;
    ConcurrentHashMap<String, Boolean> msSQLstateMap = null;

    private static String DB_URL = null;
    private static String USER = null;
    private static String PASSWORD = null;

    @GetMapping("/getQueryData")
    ResponseEntity<?> getQueryData(@PathVariable String FinalQuery) {
        ArrayList< String> statesList = null;
        CustomClass customclass = new CustomClass();
        HashMap<String, Object> resultMap = new HashMap<String, Object>();

        try {
            stateMap = getStateListFromProperty();
            statesList = new ArrayList< String>(stateMap.keySet());
            Collections.sort(statesList);
            Connection connection = null;
            double list = 0;

            ThreadPoolExecutor executor = null;
            Future future = null;
            if (statesList != null) {

                if (statesList.size() > 0) {
                    list = Math.ceil(statesList.size());
                }
                executor = (ThreadPoolExecutor) Executors.newFixedThreadPool((int) list);
            }
            // for (String stateCode : statesList) {

            for (int i = 0; i < statesList.size(); i++) {
                String stateCode = statesList.get(i);
                try {

                    JdbcTemplate StateJDBCTemplate = new JdbcTemplate(getDataSoruce(stateCode));

                    DataExtractionUtilityDaoImpl dataCollection
                            = new DataExtractionUtilityDaoImpl(StateJDBCTemplate, customclass,
                                    executor, FinalQuery);

                    future.get();
                    if (future.isDone()) {
                        future.cancel(true);
                    }
                    System.out.println("End of the ThreadPool to get the data");
//	                    String dbType = ResourceBundle.getBundle("statedb").getString(stateCode+".db.type");
//	                    String dbUrl=ResourceBundle.getBundle("statedb").getString(stateCode+".db.url");
//	                    String dbUser=ResourceBundle.getBundle("statedb").getString(stateCode+".db.user");
//	                    String dbPassword=ResourceBundle.getBundle("statedb").getString(stateCode+".db.password");
//	                    String dbClass=ResourceBundle.getBundle("statedb").getString(stateCode+".db.class");
                } catch (Exception ex) {
                    System.out.println(ex.getMessage());
                }
            }

        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        }
        if (resultMap != null) {
            resultMap.put("status", "success");
        } else {
            resultMap.put("status", "failed");
        }
        resultMap.put("result", customclass.getOutPutValueForTheQueryData());

        return ResponseEntity.ok(resultMap);
    }

    public final ConcurrentHashMap<String, Boolean> getStateListFromProperty() {
        ConcurrentHashMap<String, Boolean> stateCdMap = new ConcurrentHashMap<String, Boolean>();
        try {
            ResourceBundle dblist = ResourceBundle.getBundle("database_list");
            Enumeration<?> enm = dblist.getKeys();
            while (enm.hasMoreElements()) {
                String stateCd = "" + enm.nextElement();
                if (dblist.getString(stateCd) != null && Boolean.valueOf(dblist.getString(stateCd)) == true) {
                    stateCdMap.put(stateCd, false);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return stateCdMap;
    }

    private static DataSource getDataSoruce(String stateCode) {
    	
    	
    	 String dbType = ResourceBundle.getBundle("statedb").getString(stateCode+".db.type");
         String dbUrl=ResourceBundle.getBundle("statedb").getString(stateCode+".db.url");
         String dbUser=ResourceBundle.getBundle("statedb").getString(stateCode+".db.user");
         String dbPassword=ResourceBundle.getBundle("statedb").getString(stateCode+".db.password");
         String dbClass=ResourceBundle.getBundle("statedb").getString(stateCode+".db.class"); 
    	 
    	
    	
    	
    	
    	
    	
    	
    	
        ResourceBundle properties = ResourceBundle.getBundle("database_list");
        DriverManagerDataSource datasource = new DriverManagerDataSource();
//        DB_URL = "" + properties.getString(StateCode+".db.url");
//        USER = "" + properties.getString(StateCode+".db.user");
//        PASSWORD = "" + properties.getString(StateCode+".db.password");
       // datasource.setDriverClassName(properties.getString(StateCode+".db.class"));
        datasource.setDriverClassName(dbClass);
        datasource.setUrl(dbUrl);
        datasource.setUsername(dbUser);
        datasource.setPassword(dbPassword);
        return datasource;

    }
}
